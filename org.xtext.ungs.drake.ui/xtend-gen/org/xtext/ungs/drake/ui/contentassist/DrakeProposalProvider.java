/**
 * generated by Xtext 2.15.0
 */
package org.xtext.ungs.drake.ui.contentassist;

import org.xtext.ungs.drake.ui.contentassist.AbstractDrakeProposalProvider;

/**
 * See https://www.eclipse.org/Xtext/documentation/304_ide_concepts.html#content-assist
 * on how to customize the content assistant.
 */
@SuppressWarnings("all")
public class DrakeProposalProvider extends AbstractDrakeProposalProvider {
}
