/**
 * generated by Xtext 2.15.0
 */
package org.xtext.ungs.drake.ui;

import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.xtend.lib.annotations.FinalFieldsConstructor;
import org.xtext.ungs.drake.ui.AbstractDrakeUiModule;

/**
 * Use this class to register components to be used within the Eclipse IDE.
 */
@FinalFieldsConstructor
@SuppressWarnings("all")
public class DrakeUiModule extends AbstractDrakeUiModule {
  public DrakeUiModule(final AbstractUIPlugin plugin) {
    super(plugin);
  }
}
