package org.xtext.ungs.drake.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.xtext.ungs.drake.services.DrakeGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalDrakeParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_SIMBOL", "RULE_INT", "RULE_ID", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'max'", "'min'", "'st'", "','", "'+'", "'-'", "'*'", "'set'", "':='", "'{'", "'to'", "'}'", "'<'", "'>'", "'_'"
    };
    public static final int RULE_STRING=7;
    public static final int RULE_SIMBOL=4;
    public static final int RULE_SL_COMMENT=9;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=6;
    public static final int RULE_WS=10;
    public static final int RULE_ANY_OTHER=11;
    public static final int T__26=26;
    public static final int RULE_INT=5;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=8;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalDrakeParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalDrakeParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalDrakeParser.tokenNames; }
    public String getGrammarFileName() { return "InternalDrake.g"; }



     	private DrakeGrammarAccess grammarAccess;

        public InternalDrakeParser(TokenStream input, DrakeGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Model";
       	}

       	@Override
       	protected DrakeGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleModel"
    // InternalDrake.g:64:1: entryRuleModel returns [EObject current=null] : iv_ruleModel= ruleModel EOF ;
    public final EObject entryRuleModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleModel = null;


        try {
            // InternalDrake.g:64:46: (iv_ruleModel= ruleModel EOF )
            // InternalDrake.g:65:2: iv_ruleModel= ruleModel EOF
            {
             newCompositeNode(grammarAccess.getModelRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleModel=ruleModel();

            state._fsp--;

             current =iv_ruleModel; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleModel"


    // $ANTLR start "ruleModel"
    // InternalDrake.g:71:1: ruleModel returns [EObject current=null] : ( (lv_formulation_0_0= ruleFormulation ) )* ;
    public final EObject ruleModel() throws RecognitionException {
        EObject current = null;

        EObject lv_formulation_0_0 = null;



        	enterRule();

        try {
            // InternalDrake.g:77:2: ( ( (lv_formulation_0_0= ruleFormulation ) )* )
            // InternalDrake.g:78:2: ( (lv_formulation_0_0= ruleFormulation ) )*
            {
            // InternalDrake.g:78:2: ( (lv_formulation_0_0= ruleFormulation ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>=12 && LA1_0<=13)||LA1_0==19) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalDrake.g:79:3: (lv_formulation_0_0= ruleFormulation )
            	    {
            	    // InternalDrake.g:79:3: (lv_formulation_0_0= ruleFormulation )
            	    // InternalDrake.g:80:4: lv_formulation_0_0= ruleFormulation
            	    {

            	    				newCompositeNode(grammarAccess.getModelAccess().getFormulationFormulationParserRuleCall_0());
            	    			
            	    pushFollow(FOLLOW_3);
            	    lv_formulation_0_0=ruleFormulation();

            	    state._fsp--;


            	    				if (current==null) {
            	    					current = createModelElementForParent(grammarAccess.getModelRule());
            	    				}
            	    				add(
            	    					current,
            	    					"formulation",
            	    					lv_formulation_0_0,
            	    					"org.xtext.ungs.drake.Drake.Formulation");
            	    				afterParserOrEnumRuleCall();
            	    			

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleModel"


    // $ANTLR start "entryRuleFormulation"
    // InternalDrake.g:100:1: entryRuleFormulation returns [EObject current=null] : iv_ruleFormulation= ruleFormulation EOF ;
    public final EObject entryRuleFormulation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFormulation = null;


        try {
            // InternalDrake.g:100:52: (iv_ruleFormulation= ruleFormulation EOF )
            // InternalDrake.g:101:2: iv_ruleFormulation= ruleFormulation EOF
            {
             newCompositeNode(grammarAccess.getFormulationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFormulation=ruleFormulation();

            state._fsp--;

             current =iv_ruleFormulation; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFormulation"


    // $ANTLR start "ruleFormulation"
    // InternalDrake.g:107:1: ruleFormulation returns [EObject current=null] : ( ( (lv_definitions_0_0= ruleSet ) )* ( ( (lv_function_1_1= 'max' | lv_function_1_2= 'min' ) ) ) ( (lv_expression_2_0= ruleLinearExpression ) ) otherlv_3= 'st' ( (lv_inequation_4_0= ruleLinearInequation ) ) (otherlv_5= ',' ( (lv_inequation_6_0= ruleLinearInequation ) ) )* ) ;
    public final EObject ruleFormulation() throws RecognitionException {
        EObject current = null;

        Token lv_function_1_1=null;
        Token lv_function_1_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_definitions_0_0 = null;

        EObject lv_expression_2_0 = null;

        EObject lv_inequation_4_0 = null;

        EObject lv_inequation_6_0 = null;



        	enterRule();

        try {
            // InternalDrake.g:113:2: ( ( ( (lv_definitions_0_0= ruleSet ) )* ( ( (lv_function_1_1= 'max' | lv_function_1_2= 'min' ) ) ) ( (lv_expression_2_0= ruleLinearExpression ) ) otherlv_3= 'st' ( (lv_inequation_4_0= ruleLinearInequation ) ) (otherlv_5= ',' ( (lv_inequation_6_0= ruleLinearInequation ) ) )* ) )
            // InternalDrake.g:114:2: ( ( (lv_definitions_0_0= ruleSet ) )* ( ( (lv_function_1_1= 'max' | lv_function_1_2= 'min' ) ) ) ( (lv_expression_2_0= ruleLinearExpression ) ) otherlv_3= 'st' ( (lv_inequation_4_0= ruleLinearInequation ) ) (otherlv_5= ',' ( (lv_inequation_6_0= ruleLinearInequation ) ) )* )
            {
            // InternalDrake.g:114:2: ( ( (lv_definitions_0_0= ruleSet ) )* ( ( (lv_function_1_1= 'max' | lv_function_1_2= 'min' ) ) ) ( (lv_expression_2_0= ruleLinearExpression ) ) otherlv_3= 'st' ( (lv_inequation_4_0= ruleLinearInequation ) ) (otherlv_5= ',' ( (lv_inequation_6_0= ruleLinearInequation ) ) )* )
            // InternalDrake.g:115:3: ( (lv_definitions_0_0= ruleSet ) )* ( ( (lv_function_1_1= 'max' | lv_function_1_2= 'min' ) ) ) ( (lv_expression_2_0= ruleLinearExpression ) ) otherlv_3= 'st' ( (lv_inequation_4_0= ruleLinearInequation ) ) (otherlv_5= ',' ( (lv_inequation_6_0= ruleLinearInequation ) ) )*
            {
            // InternalDrake.g:115:3: ( (lv_definitions_0_0= ruleSet ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==19) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalDrake.g:116:4: (lv_definitions_0_0= ruleSet )
            	    {
            	    // InternalDrake.g:116:4: (lv_definitions_0_0= ruleSet )
            	    // InternalDrake.g:117:5: lv_definitions_0_0= ruleSet
            	    {

            	    					newCompositeNode(grammarAccess.getFormulationAccess().getDefinitionsSetParserRuleCall_0_0());
            	    				
            	    pushFollow(FOLLOW_4);
            	    lv_definitions_0_0=ruleSet();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getFormulationRule());
            	    					}
            	    					add(
            	    						current,
            	    						"definitions",
            	    						lv_definitions_0_0,
            	    						"org.xtext.ungs.drake.Drake.Set");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            // InternalDrake.g:134:3: ( ( (lv_function_1_1= 'max' | lv_function_1_2= 'min' ) ) )
            // InternalDrake.g:135:4: ( (lv_function_1_1= 'max' | lv_function_1_2= 'min' ) )
            {
            // InternalDrake.g:135:4: ( (lv_function_1_1= 'max' | lv_function_1_2= 'min' ) )
            // InternalDrake.g:136:5: (lv_function_1_1= 'max' | lv_function_1_2= 'min' )
            {
            // InternalDrake.g:136:5: (lv_function_1_1= 'max' | lv_function_1_2= 'min' )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==12) ) {
                alt3=1;
            }
            else if ( (LA3_0==13) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalDrake.g:137:6: lv_function_1_1= 'max'
                    {
                    lv_function_1_1=(Token)match(input,12,FOLLOW_5); 

                    						newLeafNode(lv_function_1_1, grammarAccess.getFormulationAccess().getFunctionMaxKeyword_1_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getFormulationRule());
                    						}
                    						setWithLastConsumed(current, "function", lv_function_1_1, null);
                    					

                    }
                    break;
                case 2 :
                    // InternalDrake.g:148:6: lv_function_1_2= 'min'
                    {
                    lv_function_1_2=(Token)match(input,13,FOLLOW_5); 

                    						newLeafNode(lv_function_1_2, grammarAccess.getFormulationAccess().getFunctionMinKeyword_1_0_1());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getFormulationRule());
                    						}
                    						setWithLastConsumed(current, "function", lv_function_1_2, null);
                    					

                    }
                    break;

            }


            }


            }

            // InternalDrake.g:161:3: ( (lv_expression_2_0= ruleLinearExpression ) )
            // InternalDrake.g:162:4: (lv_expression_2_0= ruleLinearExpression )
            {
            // InternalDrake.g:162:4: (lv_expression_2_0= ruleLinearExpression )
            // InternalDrake.g:163:5: lv_expression_2_0= ruleLinearExpression
            {

            					newCompositeNode(grammarAccess.getFormulationAccess().getExpressionLinearExpressionParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_6);
            lv_expression_2_0=ruleLinearExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getFormulationRule());
            					}
            					set(
            						current,
            						"expression",
            						lv_expression_2_0,
            						"org.xtext.ungs.drake.Drake.LinearExpression");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,14,FOLLOW_5); 

            			newLeafNode(otherlv_3, grammarAccess.getFormulationAccess().getStKeyword_3());
            		
            // InternalDrake.g:184:3: ( (lv_inequation_4_0= ruleLinearInequation ) )
            // InternalDrake.g:185:4: (lv_inequation_4_0= ruleLinearInequation )
            {
            // InternalDrake.g:185:4: (lv_inequation_4_0= ruleLinearInequation )
            // InternalDrake.g:186:5: lv_inequation_4_0= ruleLinearInequation
            {

            					newCompositeNode(grammarAccess.getFormulationAccess().getInequationLinearInequationParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_7);
            lv_inequation_4_0=ruleLinearInequation();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getFormulationRule());
            					}
            					add(
            						current,
            						"inequation",
            						lv_inequation_4_0,
            						"org.xtext.ungs.drake.Drake.LinearInequation");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDrake.g:203:3: (otherlv_5= ',' ( (lv_inequation_6_0= ruleLinearInequation ) ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==15) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalDrake.g:204:4: otherlv_5= ',' ( (lv_inequation_6_0= ruleLinearInequation ) )
            	    {
            	    otherlv_5=(Token)match(input,15,FOLLOW_5); 

            	    				newLeafNode(otherlv_5, grammarAccess.getFormulationAccess().getCommaKeyword_5_0());
            	    			
            	    // InternalDrake.g:208:4: ( (lv_inequation_6_0= ruleLinearInequation ) )
            	    // InternalDrake.g:209:5: (lv_inequation_6_0= ruleLinearInequation )
            	    {
            	    // InternalDrake.g:209:5: (lv_inequation_6_0= ruleLinearInequation )
            	    // InternalDrake.g:210:6: lv_inequation_6_0= ruleLinearInequation
            	    {

            	    						newCompositeNode(grammarAccess.getFormulationAccess().getInequationLinearInequationParserRuleCall_5_1_0());
            	    					
            	    pushFollow(FOLLOW_7);
            	    lv_inequation_6_0=ruleLinearInequation();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getFormulationRule());
            	    						}
            	    						add(
            	    							current,
            	    							"inequation",
            	    							lv_inequation_6_0,
            	    							"org.xtext.ungs.drake.Drake.LinearInequation");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFormulation"


    // $ANTLR start "entryRuleLinearInequation"
    // InternalDrake.g:232:1: entryRuleLinearInequation returns [EObject current=null] : iv_ruleLinearInequation= ruleLinearInequation EOF ;
    public final EObject entryRuleLinearInequation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLinearInequation = null;


        try {
            // InternalDrake.g:232:57: (iv_ruleLinearInequation= ruleLinearInequation EOF )
            // InternalDrake.g:233:2: iv_ruleLinearInequation= ruleLinearInequation EOF
            {
             newCompositeNode(grammarAccess.getLinearInequationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLinearInequation=ruleLinearInequation();

            state._fsp--;

             current =iv_ruleLinearInequation; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLinearInequation"


    // $ANTLR start "ruleLinearInequation"
    // InternalDrake.g:239:1: ruleLinearInequation returns [EObject current=null] : ( ( (lv_linearExpression_0_0= ruleLinearExpression ) ) ( (lv_simbol_1_0= RULE_SIMBOL ) ) ( ( (lv_varInequality_2_0= ruleVariable ) ) | ( (lv_intInequality_3_0= RULE_INT ) ) ) ) ;
    public final EObject ruleLinearInequation() throws RecognitionException {
        EObject current = null;

        Token lv_simbol_1_0=null;
        Token lv_intInequality_3_0=null;
        EObject lv_linearExpression_0_0 = null;

        EObject lv_varInequality_2_0 = null;



        	enterRule();

        try {
            // InternalDrake.g:245:2: ( ( ( (lv_linearExpression_0_0= ruleLinearExpression ) ) ( (lv_simbol_1_0= RULE_SIMBOL ) ) ( ( (lv_varInequality_2_0= ruleVariable ) ) | ( (lv_intInequality_3_0= RULE_INT ) ) ) ) )
            // InternalDrake.g:246:2: ( ( (lv_linearExpression_0_0= ruleLinearExpression ) ) ( (lv_simbol_1_0= RULE_SIMBOL ) ) ( ( (lv_varInequality_2_0= ruleVariable ) ) | ( (lv_intInequality_3_0= RULE_INT ) ) ) )
            {
            // InternalDrake.g:246:2: ( ( (lv_linearExpression_0_0= ruleLinearExpression ) ) ( (lv_simbol_1_0= RULE_SIMBOL ) ) ( ( (lv_varInequality_2_0= ruleVariable ) ) | ( (lv_intInequality_3_0= RULE_INT ) ) ) )
            // InternalDrake.g:247:3: ( (lv_linearExpression_0_0= ruleLinearExpression ) ) ( (lv_simbol_1_0= RULE_SIMBOL ) ) ( ( (lv_varInequality_2_0= ruleVariable ) ) | ( (lv_intInequality_3_0= RULE_INT ) ) )
            {
            // InternalDrake.g:247:3: ( (lv_linearExpression_0_0= ruleLinearExpression ) )
            // InternalDrake.g:248:4: (lv_linearExpression_0_0= ruleLinearExpression )
            {
            // InternalDrake.g:248:4: (lv_linearExpression_0_0= ruleLinearExpression )
            // InternalDrake.g:249:5: lv_linearExpression_0_0= ruleLinearExpression
            {

            					newCompositeNode(grammarAccess.getLinearInequationAccess().getLinearExpressionLinearExpressionParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_8);
            lv_linearExpression_0_0=ruleLinearExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getLinearInequationRule());
            					}
            					set(
            						current,
            						"linearExpression",
            						lv_linearExpression_0_0,
            						"org.xtext.ungs.drake.Drake.LinearExpression");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDrake.g:266:3: ( (lv_simbol_1_0= RULE_SIMBOL ) )
            // InternalDrake.g:267:4: (lv_simbol_1_0= RULE_SIMBOL )
            {
            // InternalDrake.g:267:4: (lv_simbol_1_0= RULE_SIMBOL )
            // InternalDrake.g:268:5: lv_simbol_1_0= RULE_SIMBOL
            {
            lv_simbol_1_0=(Token)match(input,RULE_SIMBOL,FOLLOW_5); 

            					newLeafNode(lv_simbol_1_0, grammarAccess.getLinearInequationAccess().getSimbolSIMBOLTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getLinearInequationRule());
            					}
            					setWithLastConsumed(
            						current,
            						"simbol",
            						lv_simbol_1_0,
            						"org.xtext.ungs.drake.Drake.SIMBOL");
            				

            }


            }

            // InternalDrake.g:284:3: ( ( (lv_varInequality_2_0= ruleVariable ) ) | ( (lv_intInequality_3_0= RULE_INT ) ) )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==RULE_ID) ) {
                alt5=1;
            }
            else if ( (LA5_0==RULE_INT) ) {
                alt5=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // InternalDrake.g:285:4: ( (lv_varInequality_2_0= ruleVariable ) )
                    {
                    // InternalDrake.g:285:4: ( (lv_varInequality_2_0= ruleVariable ) )
                    // InternalDrake.g:286:5: (lv_varInequality_2_0= ruleVariable )
                    {
                    // InternalDrake.g:286:5: (lv_varInequality_2_0= ruleVariable )
                    // InternalDrake.g:287:6: lv_varInequality_2_0= ruleVariable
                    {

                    						newCompositeNode(grammarAccess.getLinearInequationAccess().getVarInequalityVariableParserRuleCall_2_0_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_varInequality_2_0=ruleVariable();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getLinearInequationRule());
                    						}
                    						set(
                    							current,
                    							"varInequality",
                    							lv_varInequality_2_0,
                    							"org.xtext.ungs.drake.Drake.Variable");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalDrake.g:305:4: ( (lv_intInequality_3_0= RULE_INT ) )
                    {
                    // InternalDrake.g:305:4: ( (lv_intInequality_3_0= RULE_INT ) )
                    // InternalDrake.g:306:5: (lv_intInequality_3_0= RULE_INT )
                    {
                    // InternalDrake.g:306:5: (lv_intInequality_3_0= RULE_INT )
                    // InternalDrake.g:307:6: lv_intInequality_3_0= RULE_INT
                    {
                    lv_intInequality_3_0=(Token)match(input,RULE_INT,FOLLOW_2); 

                    						newLeafNode(lv_intInequality_3_0, grammarAccess.getLinearInequationAccess().getIntInequalityINTTerminalRuleCall_2_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getLinearInequationRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"intInequality",
                    							lv_intInequality_3_0,
                    							"org.eclipse.xtext.common.Terminals.INT");
                    					

                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLinearInequation"


    // $ANTLR start "entryRuleLinearExpression"
    // InternalDrake.g:328:1: entryRuleLinearExpression returns [EObject current=null] : iv_ruleLinearExpression= ruleLinearExpression EOF ;
    public final EObject entryRuleLinearExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLinearExpression = null;


        try {
            // InternalDrake.g:328:57: (iv_ruleLinearExpression= ruleLinearExpression EOF )
            // InternalDrake.g:329:2: iv_ruleLinearExpression= ruleLinearExpression EOF
            {
             newCompositeNode(grammarAccess.getLinearExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLinearExpression=ruleLinearExpression();

            state._fsp--;

             current =iv_ruleLinearExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLinearExpression"


    // $ANTLR start "ruleLinearExpression"
    // InternalDrake.g:335:1: ruleLinearExpression returns [EObject current=null] : ( ( ( (lv_factor_0_0= ruleFactor ) ) ( ( ( (lv_simbol_1_1= '+' | lv_simbol_1_2= '-' ) ) ) ( (lv_expression_2_0= ruleLinearExpression ) ) )? ) | ( (lv_value_3_0= RULE_INT ) ) ) ;
    public final EObject ruleLinearExpression() throws RecognitionException {
        EObject current = null;

        Token lv_simbol_1_1=null;
        Token lv_simbol_1_2=null;
        Token lv_value_3_0=null;
        EObject lv_factor_0_0 = null;

        EObject lv_expression_2_0 = null;



        	enterRule();

        try {
            // InternalDrake.g:341:2: ( ( ( ( (lv_factor_0_0= ruleFactor ) ) ( ( ( (lv_simbol_1_1= '+' | lv_simbol_1_2= '-' ) ) ) ( (lv_expression_2_0= ruleLinearExpression ) ) )? ) | ( (lv_value_3_0= RULE_INT ) ) ) )
            // InternalDrake.g:342:2: ( ( ( (lv_factor_0_0= ruleFactor ) ) ( ( ( (lv_simbol_1_1= '+' | lv_simbol_1_2= '-' ) ) ) ( (lv_expression_2_0= ruleLinearExpression ) ) )? ) | ( (lv_value_3_0= RULE_INT ) ) )
            {
            // InternalDrake.g:342:2: ( ( ( (lv_factor_0_0= ruleFactor ) ) ( ( ( (lv_simbol_1_1= '+' | lv_simbol_1_2= '-' ) ) ) ( (lv_expression_2_0= ruleLinearExpression ) ) )? ) | ( (lv_value_3_0= RULE_INT ) ) )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==RULE_INT) ) {
                int LA8_1 = input.LA(2);

                if ( (LA8_1==EOF||LA8_1==RULE_SIMBOL||LA8_1==14) ) {
                    alt8=2;
                }
                else if ( (LA8_1==18) ) {
                    alt8=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 8, 1, input);

                    throw nvae;
                }
            }
            else if ( (LA8_0==RULE_ID) ) {
                alt8=1;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // InternalDrake.g:343:3: ( ( (lv_factor_0_0= ruleFactor ) ) ( ( ( (lv_simbol_1_1= '+' | lv_simbol_1_2= '-' ) ) ) ( (lv_expression_2_0= ruleLinearExpression ) ) )? )
                    {
                    // InternalDrake.g:343:3: ( ( (lv_factor_0_0= ruleFactor ) ) ( ( ( (lv_simbol_1_1= '+' | lv_simbol_1_2= '-' ) ) ) ( (lv_expression_2_0= ruleLinearExpression ) ) )? )
                    // InternalDrake.g:344:4: ( (lv_factor_0_0= ruleFactor ) ) ( ( ( (lv_simbol_1_1= '+' | lv_simbol_1_2= '-' ) ) ) ( (lv_expression_2_0= ruleLinearExpression ) ) )?
                    {
                    // InternalDrake.g:344:4: ( (lv_factor_0_0= ruleFactor ) )
                    // InternalDrake.g:345:5: (lv_factor_0_0= ruleFactor )
                    {
                    // InternalDrake.g:345:5: (lv_factor_0_0= ruleFactor )
                    // InternalDrake.g:346:6: lv_factor_0_0= ruleFactor
                    {

                    						newCompositeNode(grammarAccess.getLinearExpressionAccess().getFactorFactorParserRuleCall_0_0_0());
                    					
                    pushFollow(FOLLOW_9);
                    lv_factor_0_0=ruleFactor();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getLinearExpressionRule());
                    						}
                    						set(
                    							current,
                    							"factor",
                    							lv_factor_0_0,
                    							"org.xtext.ungs.drake.Drake.Factor");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalDrake.g:363:4: ( ( ( (lv_simbol_1_1= '+' | lv_simbol_1_2= '-' ) ) ) ( (lv_expression_2_0= ruleLinearExpression ) ) )?
                    int alt7=2;
                    int LA7_0 = input.LA(1);

                    if ( ((LA7_0>=16 && LA7_0<=17)) ) {
                        alt7=1;
                    }
                    switch (alt7) {
                        case 1 :
                            // InternalDrake.g:364:5: ( ( (lv_simbol_1_1= '+' | lv_simbol_1_2= '-' ) ) ) ( (lv_expression_2_0= ruleLinearExpression ) )
                            {
                            // InternalDrake.g:364:5: ( ( (lv_simbol_1_1= '+' | lv_simbol_1_2= '-' ) ) )
                            // InternalDrake.g:365:6: ( (lv_simbol_1_1= '+' | lv_simbol_1_2= '-' ) )
                            {
                            // InternalDrake.g:365:6: ( (lv_simbol_1_1= '+' | lv_simbol_1_2= '-' ) )
                            // InternalDrake.g:366:7: (lv_simbol_1_1= '+' | lv_simbol_1_2= '-' )
                            {
                            // InternalDrake.g:366:7: (lv_simbol_1_1= '+' | lv_simbol_1_2= '-' )
                            int alt6=2;
                            int LA6_0 = input.LA(1);

                            if ( (LA6_0==16) ) {
                                alt6=1;
                            }
                            else if ( (LA6_0==17) ) {
                                alt6=2;
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 6, 0, input);

                                throw nvae;
                            }
                            switch (alt6) {
                                case 1 :
                                    // InternalDrake.g:367:8: lv_simbol_1_1= '+'
                                    {
                                    lv_simbol_1_1=(Token)match(input,16,FOLLOW_5); 

                                    								newLeafNode(lv_simbol_1_1, grammarAccess.getLinearExpressionAccess().getSimbolPlusSignKeyword_0_1_0_0_0());
                                    							

                                    								if (current==null) {
                                    									current = createModelElement(grammarAccess.getLinearExpressionRule());
                                    								}
                                    								setWithLastConsumed(current, "simbol", lv_simbol_1_1, null);
                                    							

                                    }
                                    break;
                                case 2 :
                                    // InternalDrake.g:378:8: lv_simbol_1_2= '-'
                                    {
                                    lv_simbol_1_2=(Token)match(input,17,FOLLOW_5); 

                                    								newLeafNode(lv_simbol_1_2, grammarAccess.getLinearExpressionAccess().getSimbolHyphenMinusKeyword_0_1_0_0_1());
                                    							

                                    								if (current==null) {
                                    									current = createModelElement(grammarAccess.getLinearExpressionRule());
                                    								}
                                    								setWithLastConsumed(current, "simbol", lv_simbol_1_2, null);
                                    							

                                    }
                                    break;

                            }


                            }


                            }

                            // InternalDrake.g:391:5: ( (lv_expression_2_0= ruleLinearExpression ) )
                            // InternalDrake.g:392:6: (lv_expression_2_0= ruleLinearExpression )
                            {
                            // InternalDrake.g:392:6: (lv_expression_2_0= ruleLinearExpression )
                            // InternalDrake.g:393:7: lv_expression_2_0= ruleLinearExpression
                            {

                            							newCompositeNode(grammarAccess.getLinearExpressionAccess().getExpressionLinearExpressionParserRuleCall_0_1_1_0());
                            						
                            pushFollow(FOLLOW_2);
                            lv_expression_2_0=ruleLinearExpression();

                            state._fsp--;


                            							if (current==null) {
                            								current = createModelElementForParent(grammarAccess.getLinearExpressionRule());
                            							}
                            							set(
                            								current,
                            								"expression",
                            								lv_expression_2_0,
                            								"org.xtext.ungs.drake.Drake.LinearExpression");
                            							afterParserOrEnumRuleCall();
                            						

                            }


                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalDrake.g:413:3: ( (lv_value_3_0= RULE_INT ) )
                    {
                    // InternalDrake.g:413:3: ( (lv_value_3_0= RULE_INT ) )
                    // InternalDrake.g:414:4: (lv_value_3_0= RULE_INT )
                    {
                    // InternalDrake.g:414:4: (lv_value_3_0= RULE_INT )
                    // InternalDrake.g:415:5: lv_value_3_0= RULE_INT
                    {
                    lv_value_3_0=(Token)match(input,RULE_INT,FOLLOW_2); 

                    					newLeafNode(lv_value_3_0, grammarAccess.getLinearExpressionAccess().getValueINTTerminalRuleCall_1_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getLinearExpressionRule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"value",
                    						lv_value_3_0,
                    						"org.eclipse.xtext.common.Terminals.INT");
                    				

                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLinearExpression"


    // $ANTLR start "entryRuleFactor"
    // InternalDrake.g:435:1: entryRuleFactor returns [EObject current=null] : iv_ruleFactor= ruleFactor EOF ;
    public final EObject entryRuleFactor() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFactor = null;


        try {
            // InternalDrake.g:435:47: (iv_ruleFactor= ruleFactor EOF )
            // InternalDrake.g:436:2: iv_ruleFactor= ruleFactor EOF
            {
             newCompositeNode(grammarAccess.getFactorRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFactor=ruleFactor();

            state._fsp--;

             current =iv_ruleFactor; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFactor"


    // $ANTLR start "ruleFactor"
    // InternalDrake.g:442:1: ruleFactor returns [EObject current=null] : ( ( ( (lv_value_0_0= RULE_INT ) ) ( (lv_simbolo_1_0= '*' ) ) ( (lv_variable_2_0= ruleVariable ) ) ) | ( ( (lv_variable_3_0= ruleVariable ) ) ( (lv_simbolo_4_0= '*' ) ) ( (lv_value_5_0= RULE_INT ) ) ) | ( (lv_variable_6_0= ruleVariable ) ) ) ;
    public final EObject ruleFactor() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_0=null;
        Token lv_simbolo_1_0=null;
        Token lv_simbolo_4_0=null;
        Token lv_value_5_0=null;
        EObject lv_variable_2_0 = null;

        EObject lv_variable_3_0 = null;

        EObject lv_variable_6_0 = null;



        	enterRule();

        try {
            // InternalDrake.g:448:2: ( ( ( ( (lv_value_0_0= RULE_INT ) ) ( (lv_simbolo_1_0= '*' ) ) ( (lv_variable_2_0= ruleVariable ) ) ) | ( ( (lv_variable_3_0= ruleVariable ) ) ( (lv_simbolo_4_0= '*' ) ) ( (lv_value_5_0= RULE_INT ) ) ) | ( (lv_variable_6_0= ruleVariable ) ) ) )
            // InternalDrake.g:449:2: ( ( ( (lv_value_0_0= RULE_INT ) ) ( (lv_simbolo_1_0= '*' ) ) ( (lv_variable_2_0= ruleVariable ) ) ) | ( ( (lv_variable_3_0= ruleVariable ) ) ( (lv_simbolo_4_0= '*' ) ) ( (lv_value_5_0= RULE_INT ) ) ) | ( (lv_variable_6_0= ruleVariable ) ) )
            {
            // InternalDrake.g:449:2: ( ( ( (lv_value_0_0= RULE_INT ) ) ( (lv_simbolo_1_0= '*' ) ) ( (lv_variable_2_0= ruleVariable ) ) ) | ( ( (lv_variable_3_0= ruleVariable ) ) ( (lv_simbolo_4_0= '*' ) ) ( (lv_value_5_0= RULE_INT ) ) ) | ( (lv_variable_6_0= ruleVariable ) ) )
            int alt9=3;
            alt9 = dfa9.predict(input);
            switch (alt9) {
                case 1 :
                    // InternalDrake.g:450:3: ( ( (lv_value_0_0= RULE_INT ) ) ( (lv_simbolo_1_0= '*' ) ) ( (lv_variable_2_0= ruleVariable ) ) )
                    {
                    // InternalDrake.g:450:3: ( ( (lv_value_0_0= RULE_INT ) ) ( (lv_simbolo_1_0= '*' ) ) ( (lv_variable_2_0= ruleVariable ) ) )
                    // InternalDrake.g:451:4: ( (lv_value_0_0= RULE_INT ) ) ( (lv_simbolo_1_0= '*' ) ) ( (lv_variable_2_0= ruleVariable ) )
                    {
                    // InternalDrake.g:451:4: ( (lv_value_0_0= RULE_INT ) )
                    // InternalDrake.g:452:5: (lv_value_0_0= RULE_INT )
                    {
                    // InternalDrake.g:452:5: (lv_value_0_0= RULE_INT )
                    // InternalDrake.g:453:6: lv_value_0_0= RULE_INT
                    {
                    lv_value_0_0=(Token)match(input,RULE_INT,FOLLOW_10); 

                    						newLeafNode(lv_value_0_0, grammarAccess.getFactorAccess().getValueINTTerminalRuleCall_0_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getFactorRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_0_0,
                    							"org.eclipse.xtext.common.Terminals.INT");
                    					

                    }


                    }

                    // InternalDrake.g:469:4: ( (lv_simbolo_1_0= '*' ) )
                    // InternalDrake.g:470:5: (lv_simbolo_1_0= '*' )
                    {
                    // InternalDrake.g:470:5: (lv_simbolo_1_0= '*' )
                    // InternalDrake.g:471:6: lv_simbolo_1_0= '*'
                    {
                    lv_simbolo_1_0=(Token)match(input,18,FOLLOW_11); 

                    						newLeafNode(lv_simbolo_1_0, grammarAccess.getFactorAccess().getSimboloAsteriskKeyword_0_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getFactorRule());
                    						}
                    						setWithLastConsumed(current, "simbolo", lv_simbolo_1_0, "*");
                    					

                    }


                    }

                    // InternalDrake.g:483:4: ( (lv_variable_2_0= ruleVariable ) )
                    // InternalDrake.g:484:5: (lv_variable_2_0= ruleVariable )
                    {
                    // InternalDrake.g:484:5: (lv_variable_2_0= ruleVariable )
                    // InternalDrake.g:485:6: lv_variable_2_0= ruleVariable
                    {

                    						newCompositeNode(grammarAccess.getFactorAccess().getVariableVariableParserRuleCall_0_2_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_variable_2_0=ruleVariable();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getFactorRule());
                    						}
                    						set(
                    							current,
                    							"variable",
                    							lv_variable_2_0,
                    							"org.xtext.ungs.drake.Drake.Variable");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalDrake.g:504:3: ( ( (lv_variable_3_0= ruleVariable ) ) ( (lv_simbolo_4_0= '*' ) ) ( (lv_value_5_0= RULE_INT ) ) )
                    {
                    // InternalDrake.g:504:3: ( ( (lv_variable_3_0= ruleVariable ) ) ( (lv_simbolo_4_0= '*' ) ) ( (lv_value_5_0= RULE_INT ) ) )
                    // InternalDrake.g:505:4: ( (lv_variable_3_0= ruleVariable ) ) ( (lv_simbolo_4_0= '*' ) ) ( (lv_value_5_0= RULE_INT ) )
                    {
                    // InternalDrake.g:505:4: ( (lv_variable_3_0= ruleVariable ) )
                    // InternalDrake.g:506:5: (lv_variable_3_0= ruleVariable )
                    {
                    // InternalDrake.g:506:5: (lv_variable_3_0= ruleVariable )
                    // InternalDrake.g:507:6: lv_variable_3_0= ruleVariable
                    {

                    						newCompositeNode(grammarAccess.getFactorAccess().getVariableVariableParserRuleCall_1_0_0());
                    					
                    pushFollow(FOLLOW_10);
                    lv_variable_3_0=ruleVariable();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getFactorRule());
                    						}
                    						set(
                    							current,
                    							"variable",
                    							lv_variable_3_0,
                    							"org.xtext.ungs.drake.Drake.Variable");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalDrake.g:524:4: ( (lv_simbolo_4_0= '*' ) )
                    // InternalDrake.g:525:5: (lv_simbolo_4_0= '*' )
                    {
                    // InternalDrake.g:525:5: (lv_simbolo_4_0= '*' )
                    // InternalDrake.g:526:6: lv_simbolo_4_0= '*'
                    {
                    lv_simbolo_4_0=(Token)match(input,18,FOLLOW_12); 

                    						newLeafNode(lv_simbolo_4_0, grammarAccess.getFactorAccess().getSimboloAsteriskKeyword_1_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getFactorRule());
                    						}
                    						setWithLastConsumed(current, "simbolo", lv_simbolo_4_0, "*");
                    					

                    }


                    }

                    // InternalDrake.g:538:4: ( (lv_value_5_0= RULE_INT ) )
                    // InternalDrake.g:539:5: (lv_value_5_0= RULE_INT )
                    {
                    // InternalDrake.g:539:5: (lv_value_5_0= RULE_INT )
                    // InternalDrake.g:540:6: lv_value_5_0= RULE_INT
                    {
                    lv_value_5_0=(Token)match(input,RULE_INT,FOLLOW_2); 

                    						newLeafNode(lv_value_5_0, grammarAccess.getFactorAccess().getValueINTTerminalRuleCall_1_2_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getFactorRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_5_0,
                    							"org.eclipse.xtext.common.Terminals.INT");
                    					

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalDrake.g:558:3: ( (lv_variable_6_0= ruleVariable ) )
                    {
                    // InternalDrake.g:558:3: ( (lv_variable_6_0= ruleVariable ) )
                    // InternalDrake.g:559:4: (lv_variable_6_0= ruleVariable )
                    {
                    // InternalDrake.g:559:4: (lv_variable_6_0= ruleVariable )
                    // InternalDrake.g:560:5: lv_variable_6_0= ruleVariable
                    {

                    					newCompositeNode(grammarAccess.getFactorAccess().getVariableVariableParserRuleCall_2_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_variable_6_0=ruleVariable();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getFactorRule());
                    					}
                    					set(
                    						current,
                    						"variable",
                    						lv_variable_6_0,
                    						"org.xtext.ungs.drake.Drake.Variable");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFactor"


    // $ANTLR start "entryRuleSet"
    // InternalDrake.g:581:1: entryRuleSet returns [EObject current=null] : iv_ruleSet= ruleSet EOF ;
    public final EObject entryRuleSet() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSet = null;


        try {
            // InternalDrake.g:581:44: (iv_ruleSet= ruleSet EOF )
            // InternalDrake.g:582:2: iv_ruleSet= ruleSet EOF
            {
             newCompositeNode(grammarAccess.getSetRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSet=ruleSet();

            state._fsp--;

             current =iv_ruleSet; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSet"


    // $ANTLR start "ruleSet"
    // InternalDrake.g:588:1: ruleSet returns [EObject current=null] : (otherlv_0= 'set' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':=' otherlv_3= '{' ( ( ( (lv_elements_4_0= ruleTuple ) ) (otherlv_5= ',' ( (lv_elements_6_0= ruleTuple ) ) )* )* | ( ( (lv_lowerBound_7_0= RULE_INT ) ) otherlv_8= 'to' ( (lv_upperBound_9_0= RULE_INT ) ) ) ) otherlv_10= '}' ) ;
    public final EObject ruleSet() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token lv_lowerBound_7_0=null;
        Token otherlv_8=null;
        Token lv_upperBound_9_0=null;
        Token otherlv_10=null;
        EObject lv_elements_4_0 = null;

        EObject lv_elements_6_0 = null;



        	enterRule();

        try {
            // InternalDrake.g:594:2: ( (otherlv_0= 'set' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':=' otherlv_3= '{' ( ( ( (lv_elements_4_0= ruleTuple ) ) (otherlv_5= ',' ( (lv_elements_6_0= ruleTuple ) ) )* )* | ( ( (lv_lowerBound_7_0= RULE_INT ) ) otherlv_8= 'to' ( (lv_upperBound_9_0= RULE_INT ) ) ) ) otherlv_10= '}' ) )
            // InternalDrake.g:595:2: (otherlv_0= 'set' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':=' otherlv_3= '{' ( ( ( (lv_elements_4_0= ruleTuple ) ) (otherlv_5= ',' ( (lv_elements_6_0= ruleTuple ) ) )* )* | ( ( (lv_lowerBound_7_0= RULE_INT ) ) otherlv_8= 'to' ( (lv_upperBound_9_0= RULE_INT ) ) ) ) otherlv_10= '}' )
            {
            // InternalDrake.g:595:2: (otherlv_0= 'set' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':=' otherlv_3= '{' ( ( ( (lv_elements_4_0= ruleTuple ) ) (otherlv_5= ',' ( (lv_elements_6_0= ruleTuple ) ) )* )* | ( ( (lv_lowerBound_7_0= RULE_INT ) ) otherlv_8= 'to' ( (lv_upperBound_9_0= RULE_INT ) ) ) ) otherlv_10= '}' )
            // InternalDrake.g:596:3: otherlv_0= 'set' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':=' otherlv_3= '{' ( ( ( (lv_elements_4_0= ruleTuple ) ) (otherlv_5= ',' ( (lv_elements_6_0= ruleTuple ) ) )* )* | ( ( (lv_lowerBound_7_0= RULE_INT ) ) otherlv_8= 'to' ( (lv_upperBound_9_0= RULE_INT ) ) ) ) otherlv_10= '}'
            {
            otherlv_0=(Token)match(input,19,FOLLOW_11); 

            			newLeafNode(otherlv_0, grammarAccess.getSetAccess().getSetKeyword_0());
            		
            // InternalDrake.g:600:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalDrake.g:601:4: (lv_name_1_0= RULE_ID )
            {
            // InternalDrake.g:601:4: (lv_name_1_0= RULE_ID )
            // InternalDrake.g:602:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_13); 

            					newLeafNode(lv_name_1_0, grammarAccess.getSetAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getSetRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.xtext.ungs.drake.Drake.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,20,FOLLOW_14); 

            			newLeafNode(otherlv_2, grammarAccess.getSetAccess().getColonEqualsSignKeyword_2());
            		
            otherlv_3=(Token)match(input,21,FOLLOW_15); 

            			newLeafNode(otherlv_3, grammarAccess.getSetAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalDrake.g:626:3: ( ( ( (lv_elements_4_0= ruleTuple ) ) (otherlv_5= ',' ( (lv_elements_6_0= ruleTuple ) ) )* )* | ( ( (lv_lowerBound_7_0= RULE_INT ) ) otherlv_8= 'to' ( (lv_upperBound_9_0= RULE_INT ) ) ) )
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( ((LA12_0>=23 && LA12_0<=24)) ) {
                alt12=1;
            }
            else if ( (LA12_0==RULE_INT) ) {
                alt12=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }
            switch (alt12) {
                case 1 :
                    // InternalDrake.g:627:4: ( ( (lv_elements_4_0= ruleTuple ) ) (otherlv_5= ',' ( (lv_elements_6_0= ruleTuple ) ) )* )*
                    {
                    // InternalDrake.g:627:4: ( ( (lv_elements_4_0= ruleTuple ) ) (otherlv_5= ',' ( (lv_elements_6_0= ruleTuple ) ) )* )*
                    loop11:
                    do {
                        int alt11=2;
                        int LA11_0 = input.LA(1);

                        if ( (LA11_0==24) ) {
                            alt11=1;
                        }


                        switch (alt11) {
                    	case 1 :
                    	    // InternalDrake.g:628:5: ( (lv_elements_4_0= ruleTuple ) ) (otherlv_5= ',' ( (lv_elements_6_0= ruleTuple ) ) )*
                    	    {
                    	    // InternalDrake.g:628:5: ( (lv_elements_4_0= ruleTuple ) )
                    	    // InternalDrake.g:629:6: (lv_elements_4_0= ruleTuple )
                    	    {
                    	    // InternalDrake.g:629:6: (lv_elements_4_0= ruleTuple )
                    	    // InternalDrake.g:630:7: lv_elements_4_0= ruleTuple
                    	    {

                    	    							newCompositeNode(grammarAccess.getSetAccess().getElementsTupleParserRuleCall_4_0_0_0());
                    	    						
                    	    pushFollow(FOLLOW_16);
                    	    lv_elements_4_0=ruleTuple();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getSetRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"elements",
                    	    								lv_elements_4_0,
                    	    								"org.xtext.ungs.drake.Drake.Tuple");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }

                    	    // InternalDrake.g:647:5: (otherlv_5= ',' ( (lv_elements_6_0= ruleTuple ) ) )*
                    	    loop10:
                    	    do {
                    	        int alt10=2;
                    	        int LA10_0 = input.LA(1);

                    	        if ( (LA10_0==15) ) {
                    	            alt10=1;
                    	        }


                    	        switch (alt10) {
                    	    	case 1 :
                    	    	    // InternalDrake.g:648:6: otherlv_5= ',' ( (lv_elements_6_0= ruleTuple ) )
                    	    	    {
                    	    	    otherlv_5=(Token)match(input,15,FOLLOW_17); 

                    	    	    						newLeafNode(otherlv_5, grammarAccess.getSetAccess().getCommaKeyword_4_0_1_0());
                    	    	    					
                    	    	    // InternalDrake.g:652:6: ( (lv_elements_6_0= ruleTuple ) )
                    	    	    // InternalDrake.g:653:7: (lv_elements_6_0= ruleTuple )
                    	    	    {
                    	    	    // InternalDrake.g:653:7: (lv_elements_6_0= ruleTuple )
                    	    	    // InternalDrake.g:654:8: lv_elements_6_0= ruleTuple
                    	    	    {

                    	    	    								newCompositeNode(grammarAccess.getSetAccess().getElementsTupleParserRuleCall_4_0_1_1_0());
                    	    	    							
                    	    	    pushFollow(FOLLOW_16);
                    	    	    lv_elements_6_0=ruleTuple();

                    	    	    state._fsp--;


                    	    	    								if (current==null) {
                    	    	    									current = createModelElementForParent(grammarAccess.getSetRule());
                    	    	    								}
                    	    	    								add(
                    	    	    									current,
                    	    	    									"elements",
                    	    	    									lv_elements_6_0,
                    	    	    									"org.xtext.ungs.drake.Drake.Tuple");
                    	    	    								afterParserOrEnumRuleCall();
                    	    	    							

                    	    	    }


                    	    	    }


                    	    	    }
                    	    	    break;

                    	    	default :
                    	    	    break loop10;
                    	        }
                    	    } while (true);


                    	    }
                    	    break;

                    	default :
                    	    break loop11;
                        }
                    } while (true);


                    }
                    break;
                case 2 :
                    // InternalDrake.g:674:4: ( ( (lv_lowerBound_7_0= RULE_INT ) ) otherlv_8= 'to' ( (lv_upperBound_9_0= RULE_INT ) ) )
                    {
                    // InternalDrake.g:674:4: ( ( (lv_lowerBound_7_0= RULE_INT ) ) otherlv_8= 'to' ( (lv_upperBound_9_0= RULE_INT ) ) )
                    // InternalDrake.g:675:5: ( (lv_lowerBound_7_0= RULE_INT ) ) otherlv_8= 'to' ( (lv_upperBound_9_0= RULE_INT ) )
                    {
                    // InternalDrake.g:675:5: ( (lv_lowerBound_7_0= RULE_INT ) )
                    // InternalDrake.g:676:6: (lv_lowerBound_7_0= RULE_INT )
                    {
                    // InternalDrake.g:676:6: (lv_lowerBound_7_0= RULE_INT )
                    // InternalDrake.g:677:7: lv_lowerBound_7_0= RULE_INT
                    {
                    lv_lowerBound_7_0=(Token)match(input,RULE_INT,FOLLOW_18); 

                    							newLeafNode(lv_lowerBound_7_0, grammarAccess.getSetAccess().getLowerBoundINTTerminalRuleCall_4_1_0_0());
                    						

                    							if (current==null) {
                    								current = createModelElement(grammarAccess.getSetRule());
                    							}
                    							setWithLastConsumed(
                    								current,
                    								"lowerBound",
                    								lv_lowerBound_7_0,
                    								"org.eclipse.xtext.common.Terminals.INT");
                    						

                    }


                    }

                    otherlv_8=(Token)match(input,22,FOLLOW_12); 

                    					newLeafNode(otherlv_8, grammarAccess.getSetAccess().getToKeyword_4_1_1());
                    				
                    // InternalDrake.g:697:5: ( (lv_upperBound_9_0= RULE_INT ) )
                    // InternalDrake.g:698:6: (lv_upperBound_9_0= RULE_INT )
                    {
                    // InternalDrake.g:698:6: (lv_upperBound_9_0= RULE_INT )
                    // InternalDrake.g:699:7: lv_upperBound_9_0= RULE_INT
                    {
                    lv_upperBound_9_0=(Token)match(input,RULE_INT,FOLLOW_19); 

                    							newLeafNode(lv_upperBound_9_0, grammarAccess.getSetAccess().getUpperBoundINTTerminalRuleCall_4_1_2_0());
                    						

                    							if (current==null) {
                    								current = createModelElement(grammarAccess.getSetRule());
                    							}
                    							setWithLastConsumed(
                    								current,
                    								"upperBound",
                    								lv_upperBound_9_0,
                    								"org.eclipse.xtext.common.Terminals.INT");
                    						

                    }


                    }


                    }


                    }
                    break;

            }

            otherlv_10=(Token)match(input,23,FOLLOW_2); 

            			newLeafNode(otherlv_10, grammarAccess.getSetAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSet"


    // $ANTLR start "entryRuleTuple"
    // InternalDrake.g:725:1: entryRuleTuple returns [EObject current=null] : iv_ruleTuple= ruleTuple EOF ;
    public final EObject entryRuleTuple() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTuple = null;


        try {
            // InternalDrake.g:725:46: (iv_ruleTuple= ruleTuple EOF )
            // InternalDrake.g:726:2: iv_ruleTuple= ruleTuple EOF
            {
             newCompositeNode(grammarAccess.getTupleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTuple=ruleTuple();

            state._fsp--;

             current =iv_ruleTuple; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTuple"


    // $ANTLR start "ruleTuple"
    // InternalDrake.g:732:1: ruleTuple returns [EObject current=null] : ( () otherlv_1= '<' ( ( (lv_elements_2_0= ruleElement ) ) (otherlv_3= ',' ( (lv_elements_4_0= ruleElement ) ) )* )* otherlv_5= '>' ) ;
    public final EObject ruleTuple() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_elements_2_0 = null;

        EObject lv_elements_4_0 = null;



        	enterRule();

        try {
            // InternalDrake.g:738:2: ( ( () otherlv_1= '<' ( ( (lv_elements_2_0= ruleElement ) ) (otherlv_3= ',' ( (lv_elements_4_0= ruleElement ) ) )* )* otherlv_5= '>' ) )
            // InternalDrake.g:739:2: ( () otherlv_1= '<' ( ( (lv_elements_2_0= ruleElement ) ) (otherlv_3= ',' ( (lv_elements_4_0= ruleElement ) ) )* )* otherlv_5= '>' )
            {
            // InternalDrake.g:739:2: ( () otherlv_1= '<' ( ( (lv_elements_2_0= ruleElement ) ) (otherlv_3= ',' ( (lv_elements_4_0= ruleElement ) ) )* )* otherlv_5= '>' )
            // InternalDrake.g:740:3: () otherlv_1= '<' ( ( (lv_elements_2_0= ruleElement ) ) (otherlv_3= ',' ( (lv_elements_4_0= ruleElement ) ) )* )* otherlv_5= '>'
            {
            // InternalDrake.g:740:3: ()
            // InternalDrake.g:741:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getTupleAccess().getTupleAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,24,FOLLOW_20); 

            			newLeafNode(otherlv_1, grammarAccess.getTupleAccess().getLessThanSignKeyword_1());
            		
            // InternalDrake.g:751:3: ( ( (lv_elements_2_0= ruleElement ) ) (otherlv_3= ',' ( (lv_elements_4_0= ruleElement ) ) )* )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( ((LA14_0>=RULE_INT && LA14_0<=RULE_STRING)||LA14_0==24) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // InternalDrake.g:752:4: ( (lv_elements_2_0= ruleElement ) ) (otherlv_3= ',' ( (lv_elements_4_0= ruleElement ) ) )*
            	    {
            	    // InternalDrake.g:752:4: ( (lv_elements_2_0= ruleElement ) )
            	    // InternalDrake.g:753:5: (lv_elements_2_0= ruleElement )
            	    {
            	    // InternalDrake.g:753:5: (lv_elements_2_0= ruleElement )
            	    // InternalDrake.g:754:6: lv_elements_2_0= ruleElement
            	    {

            	    						newCompositeNode(grammarAccess.getTupleAccess().getElementsElementParserRuleCall_2_0_0());
            	    					
            	    pushFollow(FOLLOW_21);
            	    lv_elements_2_0=ruleElement();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getTupleRule());
            	    						}
            	    						add(
            	    							current,
            	    							"elements",
            	    							lv_elements_2_0,
            	    							"org.xtext.ungs.drake.Drake.Element");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }

            	    // InternalDrake.g:771:4: (otherlv_3= ',' ( (lv_elements_4_0= ruleElement ) ) )*
            	    loop13:
            	    do {
            	        int alt13=2;
            	        int LA13_0 = input.LA(1);

            	        if ( (LA13_0==15) ) {
            	            alt13=1;
            	        }


            	        switch (alt13) {
            	    	case 1 :
            	    	    // InternalDrake.g:772:5: otherlv_3= ',' ( (lv_elements_4_0= ruleElement ) )
            	    	    {
            	    	    otherlv_3=(Token)match(input,15,FOLLOW_22); 

            	    	    					newLeafNode(otherlv_3, grammarAccess.getTupleAccess().getCommaKeyword_2_1_0());
            	    	    				
            	    	    // InternalDrake.g:776:5: ( (lv_elements_4_0= ruleElement ) )
            	    	    // InternalDrake.g:777:6: (lv_elements_4_0= ruleElement )
            	    	    {
            	    	    // InternalDrake.g:777:6: (lv_elements_4_0= ruleElement )
            	    	    // InternalDrake.g:778:7: lv_elements_4_0= ruleElement
            	    	    {

            	    	    							newCompositeNode(grammarAccess.getTupleAccess().getElementsElementParserRuleCall_2_1_1_0());
            	    	    						
            	    	    pushFollow(FOLLOW_21);
            	    	    lv_elements_4_0=ruleElement();

            	    	    state._fsp--;


            	    	    							if (current==null) {
            	    	    								current = createModelElementForParent(grammarAccess.getTupleRule());
            	    	    							}
            	    	    							add(
            	    	    								current,
            	    	    								"elements",
            	    	    								lv_elements_4_0,
            	    	    								"org.xtext.ungs.drake.Drake.Element");
            	    	    							afterParserOrEnumRuleCall();
            	    	    						

            	    	    }


            	    	    }


            	    	    }
            	    	    break;

            	    	default :
            	    	    break loop13;
            	        }
            	    } while (true);


            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);

            otherlv_5=(Token)match(input,25,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getTupleAccess().getGreaterThanSignKeyword_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTuple"


    // $ANTLR start "entryRuleElement"
    // InternalDrake.g:805:1: entryRuleElement returns [EObject current=null] : iv_ruleElement= ruleElement EOF ;
    public final EObject entryRuleElement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleElement = null;


        try {
            // InternalDrake.g:805:48: (iv_ruleElement= ruleElement EOF )
            // InternalDrake.g:806:2: iv_ruleElement= ruleElement EOF
            {
             newCompositeNode(grammarAccess.getElementRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleElement=ruleElement();

            state._fsp--;

             current =iv_ruleElement; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleElement"


    // $ANTLR start "ruleElement"
    // InternalDrake.g:812:1: ruleElement returns [EObject current=null] : ( ( (lv_value_0_0= RULE_INT ) ) | ( (lv_string_1_0= RULE_STRING ) ) | ( (lv_variable_2_0= ruleVariable ) ) | ( (lv_tuple_3_0= ruleTuple ) ) ) ;
    public final EObject ruleElement() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_0=null;
        Token lv_string_1_0=null;
        EObject lv_variable_2_0 = null;

        EObject lv_tuple_3_0 = null;



        	enterRule();

        try {
            // InternalDrake.g:818:2: ( ( ( (lv_value_0_0= RULE_INT ) ) | ( (lv_string_1_0= RULE_STRING ) ) | ( (lv_variable_2_0= ruleVariable ) ) | ( (lv_tuple_3_0= ruleTuple ) ) ) )
            // InternalDrake.g:819:2: ( ( (lv_value_0_0= RULE_INT ) ) | ( (lv_string_1_0= RULE_STRING ) ) | ( (lv_variable_2_0= ruleVariable ) ) | ( (lv_tuple_3_0= ruleTuple ) ) )
            {
            // InternalDrake.g:819:2: ( ( (lv_value_0_0= RULE_INT ) ) | ( (lv_string_1_0= RULE_STRING ) ) | ( (lv_variable_2_0= ruleVariable ) ) | ( (lv_tuple_3_0= ruleTuple ) ) )
            int alt15=4;
            switch ( input.LA(1) ) {
            case RULE_INT:
                {
                alt15=1;
                }
                break;
            case RULE_STRING:
                {
                alt15=2;
                }
                break;
            case RULE_ID:
                {
                alt15=3;
                }
                break;
            case 24:
                {
                alt15=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 15, 0, input);

                throw nvae;
            }

            switch (alt15) {
                case 1 :
                    // InternalDrake.g:820:3: ( (lv_value_0_0= RULE_INT ) )
                    {
                    // InternalDrake.g:820:3: ( (lv_value_0_0= RULE_INT ) )
                    // InternalDrake.g:821:4: (lv_value_0_0= RULE_INT )
                    {
                    // InternalDrake.g:821:4: (lv_value_0_0= RULE_INT )
                    // InternalDrake.g:822:5: lv_value_0_0= RULE_INT
                    {
                    lv_value_0_0=(Token)match(input,RULE_INT,FOLLOW_2); 

                    					newLeafNode(lv_value_0_0, grammarAccess.getElementAccess().getValueINTTerminalRuleCall_0_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getElementRule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"value",
                    						lv_value_0_0,
                    						"org.eclipse.xtext.common.Terminals.INT");
                    				

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalDrake.g:839:3: ( (lv_string_1_0= RULE_STRING ) )
                    {
                    // InternalDrake.g:839:3: ( (lv_string_1_0= RULE_STRING ) )
                    // InternalDrake.g:840:4: (lv_string_1_0= RULE_STRING )
                    {
                    // InternalDrake.g:840:4: (lv_string_1_0= RULE_STRING )
                    // InternalDrake.g:841:5: lv_string_1_0= RULE_STRING
                    {
                    lv_string_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    					newLeafNode(lv_string_1_0, grammarAccess.getElementAccess().getStringSTRINGTerminalRuleCall_1_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getElementRule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"string",
                    						lv_string_1_0,
                    						"org.eclipse.xtext.common.Terminals.STRING");
                    				

                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalDrake.g:858:3: ( (lv_variable_2_0= ruleVariable ) )
                    {
                    // InternalDrake.g:858:3: ( (lv_variable_2_0= ruleVariable ) )
                    // InternalDrake.g:859:4: (lv_variable_2_0= ruleVariable )
                    {
                    // InternalDrake.g:859:4: (lv_variable_2_0= ruleVariable )
                    // InternalDrake.g:860:5: lv_variable_2_0= ruleVariable
                    {

                    					newCompositeNode(grammarAccess.getElementAccess().getVariableVariableParserRuleCall_2_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_variable_2_0=ruleVariable();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getElementRule());
                    					}
                    					set(
                    						current,
                    						"variable",
                    						lv_variable_2_0,
                    						"org.xtext.ungs.drake.Drake.Variable");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;
                case 4 :
                    // InternalDrake.g:878:3: ( (lv_tuple_3_0= ruleTuple ) )
                    {
                    // InternalDrake.g:878:3: ( (lv_tuple_3_0= ruleTuple ) )
                    // InternalDrake.g:879:4: (lv_tuple_3_0= ruleTuple )
                    {
                    // InternalDrake.g:879:4: (lv_tuple_3_0= ruleTuple )
                    // InternalDrake.g:880:5: lv_tuple_3_0= ruleTuple
                    {

                    					newCompositeNode(grammarAccess.getElementAccess().getTupleTupleParserRuleCall_3_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_tuple_3_0=ruleTuple();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getElementRule());
                    					}
                    					set(
                    						current,
                    						"tuple",
                    						lv_tuple_3_0,
                    						"org.xtext.ungs.drake.Drake.Tuple");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleElement"


    // $ANTLR start "entryRuleVariable"
    // InternalDrake.g:901:1: entryRuleVariable returns [EObject current=null] : iv_ruleVariable= ruleVariable EOF ;
    public final EObject entryRuleVariable() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariable = null;


        try {
            // InternalDrake.g:901:49: (iv_ruleVariable= ruleVariable EOF )
            // InternalDrake.g:902:2: iv_ruleVariable= ruleVariable EOF
            {
             newCompositeNode(grammarAccess.getVariableRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleVariable=ruleVariable();

            state._fsp--;

             current =iv_ruleVariable; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariable"


    // $ANTLR start "ruleVariable"
    // InternalDrake.g:908:1: ruleVariable returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) (otherlv_1= '_' ( ( (lv_index_2_0= RULE_INT ) ) | (otherlv_3= '{' ( (lv_index_4_0= RULE_INT ) ) (otherlv_5= ',' ( (lv_index_6_0= RULE_INT ) ) )? otherlv_7= '}' ) ) )? ) ;
    public final EObject ruleVariable() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token lv_index_2_0=null;
        Token otherlv_3=null;
        Token lv_index_4_0=null;
        Token otherlv_5=null;
        Token lv_index_6_0=null;
        Token otherlv_7=null;


        	enterRule();

        try {
            // InternalDrake.g:914:2: ( ( ( (lv_name_0_0= RULE_ID ) ) (otherlv_1= '_' ( ( (lv_index_2_0= RULE_INT ) ) | (otherlv_3= '{' ( (lv_index_4_0= RULE_INT ) ) (otherlv_5= ',' ( (lv_index_6_0= RULE_INT ) ) )? otherlv_7= '}' ) ) )? ) )
            // InternalDrake.g:915:2: ( ( (lv_name_0_0= RULE_ID ) ) (otherlv_1= '_' ( ( (lv_index_2_0= RULE_INT ) ) | (otherlv_3= '{' ( (lv_index_4_0= RULE_INT ) ) (otherlv_5= ',' ( (lv_index_6_0= RULE_INT ) ) )? otherlv_7= '}' ) ) )? )
            {
            // InternalDrake.g:915:2: ( ( (lv_name_0_0= RULE_ID ) ) (otherlv_1= '_' ( ( (lv_index_2_0= RULE_INT ) ) | (otherlv_3= '{' ( (lv_index_4_0= RULE_INT ) ) (otherlv_5= ',' ( (lv_index_6_0= RULE_INT ) ) )? otherlv_7= '}' ) ) )? )
            // InternalDrake.g:916:3: ( (lv_name_0_0= RULE_ID ) ) (otherlv_1= '_' ( ( (lv_index_2_0= RULE_INT ) ) | (otherlv_3= '{' ( (lv_index_4_0= RULE_INT ) ) (otherlv_5= ',' ( (lv_index_6_0= RULE_INT ) ) )? otherlv_7= '}' ) ) )?
            {
            // InternalDrake.g:916:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalDrake.g:917:4: (lv_name_0_0= RULE_ID )
            {
            // InternalDrake.g:917:4: (lv_name_0_0= RULE_ID )
            // InternalDrake.g:918:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_23); 

            					newLeafNode(lv_name_0_0, grammarAccess.getVariableAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getVariableRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.xtext.ungs.drake.Drake.ID");
            				

            }


            }

            // InternalDrake.g:934:3: (otherlv_1= '_' ( ( (lv_index_2_0= RULE_INT ) ) | (otherlv_3= '{' ( (lv_index_4_0= RULE_INT ) ) (otherlv_5= ',' ( (lv_index_6_0= RULE_INT ) ) )? otherlv_7= '}' ) ) )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==26) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalDrake.g:935:4: otherlv_1= '_' ( ( (lv_index_2_0= RULE_INT ) ) | (otherlv_3= '{' ( (lv_index_4_0= RULE_INT ) ) (otherlv_5= ',' ( (lv_index_6_0= RULE_INT ) ) )? otherlv_7= '}' ) )
                    {
                    otherlv_1=(Token)match(input,26,FOLLOW_24); 

                    				newLeafNode(otherlv_1, grammarAccess.getVariableAccess().get_Keyword_1_0());
                    			
                    // InternalDrake.g:939:4: ( ( (lv_index_2_0= RULE_INT ) ) | (otherlv_3= '{' ( (lv_index_4_0= RULE_INT ) ) (otherlv_5= ',' ( (lv_index_6_0= RULE_INT ) ) )? otherlv_7= '}' ) )
                    int alt17=2;
                    int LA17_0 = input.LA(1);

                    if ( (LA17_0==RULE_INT) ) {
                        alt17=1;
                    }
                    else if ( (LA17_0==21) ) {
                        alt17=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 17, 0, input);

                        throw nvae;
                    }
                    switch (alt17) {
                        case 1 :
                            // InternalDrake.g:940:5: ( (lv_index_2_0= RULE_INT ) )
                            {
                            // InternalDrake.g:940:5: ( (lv_index_2_0= RULE_INT ) )
                            // InternalDrake.g:941:6: (lv_index_2_0= RULE_INT )
                            {
                            // InternalDrake.g:941:6: (lv_index_2_0= RULE_INT )
                            // InternalDrake.g:942:7: lv_index_2_0= RULE_INT
                            {
                            lv_index_2_0=(Token)match(input,RULE_INT,FOLLOW_2); 

                            							newLeafNode(lv_index_2_0, grammarAccess.getVariableAccess().getIndexINTTerminalRuleCall_1_1_0_0());
                            						

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getVariableRule());
                            							}
                            							addWithLastConsumed(
                            								current,
                            								"index",
                            								lv_index_2_0,
                            								"org.eclipse.xtext.common.Terminals.INT");
                            						

                            }


                            }


                            }
                            break;
                        case 2 :
                            // InternalDrake.g:959:5: (otherlv_3= '{' ( (lv_index_4_0= RULE_INT ) ) (otherlv_5= ',' ( (lv_index_6_0= RULE_INT ) ) )? otherlv_7= '}' )
                            {
                            // InternalDrake.g:959:5: (otherlv_3= '{' ( (lv_index_4_0= RULE_INT ) ) (otherlv_5= ',' ( (lv_index_6_0= RULE_INT ) ) )? otherlv_7= '}' )
                            // InternalDrake.g:960:6: otherlv_3= '{' ( (lv_index_4_0= RULE_INT ) ) (otherlv_5= ',' ( (lv_index_6_0= RULE_INT ) ) )? otherlv_7= '}'
                            {
                            otherlv_3=(Token)match(input,21,FOLLOW_12); 

                            						newLeafNode(otherlv_3, grammarAccess.getVariableAccess().getLeftCurlyBracketKeyword_1_1_1_0());
                            					
                            // InternalDrake.g:964:6: ( (lv_index_4_0= RULE_INT ) )
                            // InternalDrake.g:965:7: (lv_index_4_0= RULE_INT )
                            {
                            // InternalDrake.g:965:7: (lv_index_4_0= RULE_INT )
                            // InternalDrake.g:966:8: lv_index_4_0= RULE_INT
                            {
                            lv_index_4_0=(Token)match(input,RULE_INT,FOLLOW_25); 

                            								newLeafNode(lv_index_4_0, grammarAccess.getVariableAccess().getIndexINTTerminalRuleCall_1_1_1_1_0());
                            							

                            								if (current==null) {
                            									current = createModelElement(grammarAccess.getVariableRule());
                            								}
                            								addWithLastConsumed(
                            									current,
                            									"index",
                            									lv_index_4_0,
                            									"org.eclipse.xtext.common.Terminals.INT");
                            							

                            }


                            }

                            // InternalDrake.g:982:6: (otherlv_5= ',' ( (lv_index_6_0= RULE_INT ) ) )?
                            int alt16=2;
                            int LA16_0 = input.LA(1);

                            if ( (LA16_0==15) ) {
                                alt16=1;
                            }
                            switch (alt16) {
                                case 1 :
                                    // InternalDrake.g:983:7: otherlv_5= ',' ( (lv_index_6_0= RULE_INT ) )
                                    {
                                    otherlv_5=(Token)match(input,15,FOLLOW_12); 

                                    							newLeafNode(otherlv_5, grammarAccess.getVariableAccess().getCommaKeyword_1_1_1_2_0());
                                    						
                                    // InternalDrake.g:987:7: ( (lv_index_6_0= RULE_INT ) )
                                    // InternalDrake.g:988:8: (lv_index_6_0= RULE_INT )
                                    {
                                    // InternalDrake.g:988:8: (lv_index_6_0= RULE_INT )
                                    // InternalDrake.g:989:9: lv_index_6_0= RULE_INT
                                    {
                                    lv_index_6_0=(Token)match(input,RULE_INT,FOLLOW_19); 

                                    									newLeafNode(lv_index_6_0, grammarAccess.getVariableAccess().getIndexINTTerminalRuleCall_1_1_1_2_1_0());
                                    								

                                    									if (current==null) {
                                    										current = createModelElement(grammarAccess.getVariableRule());
                                    									}
                                    									addWithLastConsumed(
                                    										current,
                                    										"index",
                                    										lv_index_6_0,
                                    										"org.eclipse.xtext.common.Terminals.INT");
                                    								

                                    }


                                    }


                                    }
                                    break;

                            }

                            otherlv_7=(Token)match(input,23,FOLLOW_2); 

                            						newLeafNode(otherlv_7, grammarAccess.getVariableAccess().getRightCurlyBracketKeyword_1_1_1_3());
                            					

                            }


                            }
                            break;

                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariable"

    // Delegated rules


    protected DFA9 dfa9 = new DFA9(this);
    static final String dfa_1s = "\14\uffff";
    static final String dfa_2s = "\2\uffff\1\5\3\uffff\1\5\3\uffff\1\5\1\uffff";
    static final String dfa_3s = "\1\5\1\uffff\1\4\1\5\2\uffff\1\4\1\5\1\17\1\5\1\4\1\27";
    static final String dfa_4s = "\1\6\1\uffff\1\32\1\25\2\uffff\1\22\1\5\1\27\1\5\1\22\1\27";
    static final String dfa_5s = "\1\uffff\1\1\2\uffff\1\2\1\3\6\uffff";
    static final String dfa_6s = "\14\uffff}>";
    static final String[] dfa_7s = {
            "\1\1\1\2",
            "",
            "\1\5\11\uffff\1\5\1\uffff\2\5\1\4\7\uffff\1\3",
            "\1\6\17\uffff\1\7",
            "",
            "",
            "\1\5\11\uffff\1\5\1\uffff\2\5\1\4",
            "\1\10",
            "\1\11\7\uffff\1\12",
            "\1\13",
            "\1\5\11\uffff\1\5\1\uffff\2\5\1\4",
            "\1\12"
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final short[] dfa_2 = DFA.unpackEncodedString(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final char[] dfa_4 = DFA.unpackEncodedStringToUnsignedChars(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[] dfa_6 = DFA.unpackEncodedString(dfa_6s);
    static final short[][] dfa_7 = unpackEncodedStringArray(dfa_7s);

    class DFA9 extends DFA {

        public DFA9(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 9;
            this.eot = dfa_1;
            this.eof = dfa_2;
            this.min = dfa_3;
            this.max = dfa_4;
            this.accept = dfa_5;
            this.special = dfa_6;
            this.transition = dfa_7;
        }
        public String getDescription() {
            return "449:2: ( ( ( (lv_value_0_0= RULE_INT ) ) ( (lv_simbolo_1_0= '*' ) ) ( (lv_variable_2_0= ruleVariable ) ) ) | ( ( (lv_variable_3_0= ruleVariable ) ) ( (lv_simbolo_4_0= '*' ) ) ( (lv_value_5_0= RULE_INT ) ) ) | ( (lv_variable_6_0= ruleVariable ) ) )";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000083002L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000083000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000060L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000008002L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000030002L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000001800020L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000001808000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x00000000030000E0L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x00000000030080E0L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x00000000010000E0L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000004000002L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000000200020L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000000808000L});

}