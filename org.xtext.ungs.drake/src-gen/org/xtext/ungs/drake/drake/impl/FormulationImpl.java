/**
 * generated by Xtext 2.16.0
 */
package org.xtext.ungs.drake.drake.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.xtext.ungs.drake.drake.DrakePackage;
import org.xtext.ungs.drake.drake.Formulation;
import org.xtext.ungs.drake.drake.LinearExpression;
import org.xtext.ungs.drake.drake.LinearInequation;
import org.xtext.ungs.drake.drake.Set;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Formulation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.ungs.drake.drake.impl.FormulationImpl#getDefinitions <em>Definitions</em>}</li>
 *   <li>{@link org.xtext.ungs.drake.drake.impl.FormulationImpl#getFunction <em>Function</em>}</li>
 *   <li>{@link org.xtext.ungs.drake.drake.impl.FormulationImpl#getExpression <em>Expression</em>}</li>
 *   <li>{@link org.xtext.ungs.drake.drake.impl.FormulationImpl#getInequation <em>Inequation</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FormulationImpl extends MinimalEObjectImpl.Container implements Formulation
{
  /**
   * The cached value of the '{@link #getDefinitions() <em>Definitions</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDefinitions()
   * @generated
   * @ordered
   */
  protected EList<Set> definitions;

  /**
   * The default value of the '{@link #getFunction() <em>Function</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFunction()
   * @generated
   * @ordered
   */
  protected static final String FUNCTION_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getFunction() <em>Function</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFunction()
   * @generated
   * @ordered
   */
  protected String function = FUNCTION_EDEFAULT;

  /**
   * The cached value of the '{@link #getExpression() <em>Expression</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExpression()
   * @generated
   * @ordered
   */
  protected LinearExpression expression;

  /**
   * The cached value of the '{@link #getInequation() <em>Inequation</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getInequation()
   * @generated
   * @ordered
   */
  protected EList<LinearInequation> inequation;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected FormulationImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return DrakePackage.Literals.FORMULATION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Set> getDefinitions()
  {
    if (definitions == null)
    {
      definitions = new EObjectContainmentEList<Set>(Set.class, this, DrakePackage.FORMULATION__DEFINITIONS);
    }
    return definitions;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getFunction()
  {
    return function;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFunction(String newFunction)
  {
    String oldFunction = function;
    function = newFunction;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DrakePackage.FORMULATION__FUNCTION, oldFunction, function));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LinearExpression getExpression()
  {
    return expression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetExpression(LinearExpression newExpression, NotificationChain msgs)
  {
    LinearExpression oldExpression = expression;
    expression = newExpression;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DrakePackage.FORMULATION__EXPRESSION, oldExpression, newExpression);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setExpression(LinearExpression newExpression)
  {
    if (newExpression != expression)
    {
      NotificationChain msgs = null;
      if (expression != null)
        msgs = ((InternalEObject)expression).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DrakePackage.FORMULATION__EXPRESSION, null, msgs);
      if (newExpression != null)
        msgs = ((InternalEObject)newExpression).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DrakePackage.FORMULATION__EXPRESSION, null, msgs);
      msgs = basicSetExpression(newExpression, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DrakePackage.FORMULATION__EXPRESSION, newExpression, newExpression));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<LinearInequation> getInequation()
  {
    if (inequation == null)
    {
      inequation = new EObjectContainmentEList<LinearInequation>(LinearInequation.class, this, DrakePackage.FORMULATION__INEQUATION);
    }
    return inequation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case DrakePackage.FORMULATION__DEFINITIONS:
        return ((InternalEList<?>)getDefinitions()).basicRemove(otherEnd, msgs);
      case DrakePackage.FORMULATION__EXPRESSION:
        return basicSetExpression(null, msgs);
      case DrakePackage.FORMULATION__INEQUATION:
        return ((InternalEList<?>)getInequation()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case DrakePackage.FORMULATION__DEFINITIONS:
        return getDefinitions();
      case DrakePackage.FORMULATION__FUNCTION:
        return getFunction();
      case DrakePackage.FORMULATION__EXPRESSION:
        return getExpression();
      case DrakePackage.FORMULATION__INEQUATION:
        return getInequation();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case DrakePackage.FORMULATION__DEFINITIONS:
        getDefinitions().clear();
        getDefinitions().addAll((Collection<? extends Set>)newValue);
        return;
      case DrakePackage.FORMULATION__FUNCTION:
        setFunction((String)newValue);
        return;
      case DrakePackage.FORMULATION__EXPRESSION:
        setExpression((LinearExpression)newValue);
        return;
      case DrakePackage.FORMULATION__INEQUATION:
        getInequation().clear();
        getInequation().addAll((Collection<? extends LinearInequation>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case DrakePackage.FORMULATION__DEFINITIONS:
        getDefinitions().clear();
        return;
      case DrakePackage.FORMULATION__FUNCTION:
        setFunction(FUNCTION_EDEFAULT);
        return;
      case DrakePackage.FORMULATION__EXPRESSION:
        setExpression((LinearExpression)null);
        return;
      case DrakePackage.FORMULATION__INEQUATION:
        getInequation().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case DrakePackage.FORMULATION__DEFINITIONS:
        return definitions != null && !definitions.isEmpty();
      case DrakePackage.FORMULATION__FUNCTION:
        return FUNCTION_EDEFAULT == null ? function != null : !FUNCTION_EDEFAULT.equals(function);
      case DrakePackage.FORMULATION__EXPRESSION:
        return expression != null;
      case DrakePackage.FORMULATION__INEQUATION:
        return inequation != null && !inequation.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuilder result = new StringBuilder(super.toString());
    result.append(" (function: ");
    result.append(function);
    result.append(')');
    return result.toString();
  }

} //FormulationImpl
