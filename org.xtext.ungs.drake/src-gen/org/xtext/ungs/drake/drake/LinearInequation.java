/**
 * generated by Xtext 2.16.0
 */
package org.xtext.ungs.drake.drake;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Linear Inequation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.ungs.drake.drake.LinearInequation#getLinearExpression <em>Linear Expression</em>}</li>
 *   <li>{@link org.xtext.ungs.drake.drake.LinearInequation#getSimbol <em>Simbol</em>}</li>
 *   <li>{@link org.xtext.ungs.drake.drake.LinearInequation#getVarInequality <em>Var Inequality</em>}</li>
 *   <li>{@link org.xtext.ungs.drake.drake.LinearInequation#getIntInequality <em>Int Inequality</em>}</li>
 * </ul>
 *
 * @see org.xtext.ungs.drake.drake.DrakePackage#getLinearInequation()
 * @model
 * @generated
 */
public interface LinearInequation extends EObject
{
  /**
   * Returns the value of the '<em><b>Linear Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Linear Expression</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Linear Expression</em>' containment reference.
   * @see #setLinearExpression(LinearExpression)
   * @see org.xtext.ungs.drake.drake.DrakePackage#getLinearInequation_LinearExpression()
   * @model containment="true"
   * @generated
   */
  LinearExpression getLinearExpression();

  /**
   * Sets the value of the '{@link org.xtext.ungs.drake.drake.LinearInequation#getLinearExpression <em>Linear Expression</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Linear Expression</em>' containment reference.
   * @see #getLinearExpression()
   * @generated
   */
  void setLinearExpression(LinearExpression value);

  /**
   * Returns the value of the '<em><b>Simbol</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Simbol</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Simbol</em>' attribute.
   * @see #setSimbol(String)
   * @see org.xtext.ungs.drake.drake.DrakePackage#getLinearInequation_Simbol()
   * @model
   * @generated
   */
  String getSimbol();

  /**
   * Sets the value of the '{@link org.xtext.ungs.drake.drake.LinearInequation#getSimbol <em>Simbol</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Simbol</em>' attribute.
   * @see #getSimbol()
   * @generated
   */
  void setSimbol(String value);

  /**
   * Returns the value of the '<em><b>Var Inequality</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Var Inequality</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Var Inequality</em>' containment reference.
   * @see #setVarInequality(Variable)
   * @see org.xtext.ungs.drake.drake.DrakePackage#getLinearInequation_VarInequality()
   * @model containment="true"
   * @generated
   */
  Variable getVarInequality();

  /**
   * Sets the value of the '{@link org.xtext.ungs.drake.drake.LinearInequation#getVarInequality <em>Var Inequality</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Var Inequality</em>' containment reference.
   * @see #getVarInequality()
   * @generated
   */
  void setVarInequality(Variable value);

  /**
   * Returns the value of the '<em><b>Int Inequality</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Int Inequality</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Int Inequality</em>' attribute.
   * @see #setIntInequality(int)
   * @see org.xtext.ungs.drake.drake.DrakePackage#getLinearInequation_IntInequality()
   * @model
   * @generated
   */
  int getIntInequality();

  /**
   * Sets the value of the '{@link org.xtext.ungs.drake.drake.LinearInequation#getIntInequality <em>Int Inequality</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Int Inequality</em>' attribute.
   * @see #getIntInequality()
   * @generated
   */
  void setIntInequality(int value);

} // LinearInequation
