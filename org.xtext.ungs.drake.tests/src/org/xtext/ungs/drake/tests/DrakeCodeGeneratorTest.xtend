package org.xtext.ungs.drake.tests
 
import org.eclipse.xtext.testing.InjectWith
import org.junit.Test
import org.junit.runner.RunWith
import org.eclipse.xtext.testing.XtextRunner
import com.google.inject.Inject
import org.eclipse.xtext.xbase.testing.CompilationTestHelper

@RunWith(XtextRunner)
@InjectWith(DrakeInjectorProvider)
class DrakeCodeGeneratorTest {
	@Inject extension CompilationTestHelper
	
	@Test
	def void generateValidCode1(){
		'''
		max 10 * x + 2 st x >= 10
		'''.assertCompilesTo('''
		max 10 * x + 2 st x >= 10
		''')
	}
	
	@Test
	def void generateValidCode2(){
		'''
		max x_{3,4} * 10 st x >= 10, y_{3} = 10, x <= 10
		'''.assertCompilesTo('''
		max 10 * x_{3,4} st x >= 10, y_{3} = 10, x <= 10
		''')
	}
	
}