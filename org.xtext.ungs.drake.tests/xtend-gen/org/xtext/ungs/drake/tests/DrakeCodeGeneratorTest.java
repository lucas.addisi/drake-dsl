package org.xtext.ungs.drake.tests;

import com.google.inject.Inject;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.testing.InjectWith;
import org.eclipse.xtext.testing.XtextRunner;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.testing.CompilationTestHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.xtext.ungs.drake.tests.DrakeInjectorProvider;

@RunWith(XtextRunner.class)
@InjectWith(DrakeInjectorProvider.class)
@SuppressWarnings("all")
public class DrakeCodeGeneratorTest {
  @Inject
  @Extension
  private CompilationTestHelper _compilationTestHelper;
  
  @Test
  public void generateValidCode1() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("max 10 * x + 2 st x >= 10");
      _builder.newLine();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("max 10 * x + 2 st x >= 10");
      _builder_1.newLine();
      this._compilationTestHelper.assertCompilesTo(_builder, _builder_1);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void generateValidCode2() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("max x_{3,4} * 10 st x >= 10, y_{3} = 10, x <= 10");
      _builder.newLine();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("max 10 * x_{3,4} st x >= 10, y_{3} = 10, x <= 10");
      _builder_1.newLine();
      this._compilationTestHelper.assertCompilesTo(_builder, _builder_1);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
