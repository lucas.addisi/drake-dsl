/*
 * generated by Xtext 2.16.0
 */
package org.xtext.ungs.drake.ide.contentassist.antlr;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.Map;
import org.eclipse.xtext.AbstractElement;
import org.eclipse.xtext.ide.editor.contentassist.antlr.AbstractContentAssistParser;
import org.xtext.ungs.drake.ide.contentassist.antlr.internal.InternalDrakeParser;
import org.xtext.ungs.drake.services.DrakeGrammarAccess;

public class DrakeParser extends AbstractContentAssistParser {

	@Singleton
	public static final class NameMappings {
		
		private final Map<AbstractElement, String> mappings;
		
		@Inject
		public NameMappings(DrakeGrammarAccess grammarAccess) {
			ImmutableMap.Builder<AbstractElement, String> builder = ImmutableMap.builder();
			init(builder, grammarAccess);
			this.mappings = builder.build();
		}
		
		public String getRuleName(AbstractElement element) {
			return mappings.get(element);
		}
		
		private static void init(ImmutableMap.Builder<AbstractElement, String> builder, DrakeGrammarAccess grammarAccess) {
			builder.put(grammarAccess.getFormulationAccess().getFunctionAlternatives_1_0(), "rule__Formulation__FunctionAlternatives_1_0");
			builder.put(grammarAccess.getLinearInequationAccess().getAlternatives_2(), "rule__LinearInequation__Alternatives_2");
			builder.put(grammarAccess.getLinearExpressionAccess().getAlternatives(), "rule__LinearExpression__Alternatives");
			builder.put(grammarAccess.getLinearExpressionAccess().getSimbolAlternatives_0_1_0_0(), "rule__LinearExpression__SimbolAlternatives_0_1_0_0");
			builder.put(grammarAccess.getFactorAccess().getAlternatives(), "rule__Factor__Alternatives");
			builder.put(grammarAccess.getSetAccess().getAlternatives_4(), "rule__Set__Alternatives_4");
			builder.put(grammarAccess.getElementAccess().getAlternatives(), "rule__Element__Alternatives");
			builder.put(grammarAccess.getVariableAccess().getAlternatives_1_1(), "rule__Variable__Alternatives_1_1");
			builder.put(grammarAccess.getFormulationAccess().getGroup(), "rule__Formulation__Group__0");
			builder.put(grammarAccess.getFormulationAccess().getGroup_5(), "rule__Formulation__Group_5__0");
			builder.put(grammarAccess.getLinearInequationAccess().getGroup(), "rule__LinearInequation__Group__0");
			builder.put(grammarAccess.getLinearExpressionAccess().getGroup_0(), "rule__LinearExpression__Group_0__0");
			builder.put(grammarAccess.getLinearExpressionAccess().getGroup_0_1(), "rule__LinearExpression__Group_0_1__0");
			builder.put(grammarAccess.getFactorAccess().getGroup_0(), "rule__Factor__Group_0__0");
			builder.put(grammarAccess.getFactorAccess().getGroup_1(), "rule__Factor__Group_1__0");
			builder.put(grammarAccess.getSetAccess().getGroup(), "rule__Set__Group__0");
			builder.put(grammarAccess.getSetAccess().getGroup_4_0(), "rule__Set__Group_4_0__0");
			builder.put(grammarAccess.getSetAccess().getGroup_4_0_1(), "rule__Set__Group_4_0_1__0");
			builder.put(grammarAccess.getSetAccess().getGroup_4_1(), "rule__Set__Group_4_1__0");
			builder.put(grammarAccess.getTupleAccess().getGroup(), "rule__Tuple__Group__0");
			builder.put(grammarAccess.getTupleAccess().getGroup_2(), "rule__Tuple__Group_2__0");
			builder.put(grammarAccess.getTupleAccess().getGroup_2_1(), "rule__Tuple__Group_2_1__0");
			builder.put(grammarAccess.getVariableAccess().getGroup(), "rule__Variable__Group__0");
			builder.put(grammarAccess.getVariableAccess().getGroup_1(), "rule__Variable__Group_1__0");
			builder.put(grammarAccess.getVariableAccess().getGroup_1_1_1(), "rule__Variable__Group_1_1_1__0");
			builder.put(grammarAccess.getVariableAccess().getGroup_1_1_1_2(), "rule__Variable__Group_1_1_1_2__0");
			builder.put(grammarAccess.getModelAccess().getFormulationAssignment(), "rule__Model__FormulationAssignment");
			builder.put(grammarAccess.getFormulationAccess().getDefinitionsAssignment_0(), "rule__Formulation__DefinitionsAssignment_0");
			builder.put(grammarAccess.getFormulationAccess().getFunctionAssignment_1(), "rule__Formulation__FunctionAssignment_1");
			builder.put(grammarAccess.getFormulationAccess().getExpressionAssignment_2(), "rule__Formulation__ExpressionAssignment_2");
			builder.put(grammarAccess.getFormulationAccess().getInequationAssignment_4(), "rule__Formulation__InequationAssignment_4");
			builder.put(grammarAccess.getFormulationAccess().getInequationAssignment_5_1(), "rule__Formulation__InequationAssignment_5_1");
			builder.put(grammarAccess.getLinearInequationAccess().getLinearExpressionAssignment_0(), "rule__LinearInequation__LinearExpressionAssignment_0");
			builder.put(grammarAccess.getLinearInequationAccess().getSimbolAssignment_1(), "rule__LinearInequation__SimbolAssignment_1");
			builder.put(grammarAccess.getLinearInequationAccess().getVarInequalityAssignment_2_0(), "rule__LinearInequation__VarInequalityAssignment_2_0");
			builder.put(grammarAccess.getLinearInequationAccess().getIntInequalityAssignment_2_1(), "rule__LinearInequation__IntInequalityAssignment_2_1");
			builder.put(grammarAccess.getLinearExpressionAccess().getFactorAssignment_0_0(), "rule__LinearExpression__FactorAssignment_0_0");
			builder.put(grammarAccess.getLinearExpressionAccess().getSimbolAssignment_0_1_0(), "rule__LinearExpression__SimbolAssignment_0_1_0");
			builder.put(grammarAccess.getLinearExpressionAccess().getExpressionAssignment_0_1_1(), "rule__LinearExpression__ExpressionAssignment_0_1_1");
			builder.put(grammarAccess.getLinearExpressionAccess().getValueAssignment_1(), "rule__LinearExpression__ValueAssignment_1");
			builder.put(grammarAccess.getFactorAccess().getValueAssignment_0_0(), "rule__Factor__ValueAssignment_0_0");
			builder.put(grammarAccess.getFactorAccess().getSimboloAssignment_0_1(), "rule__Factor__SimboloAssignment_0_1");
			builder.put(grammarAccess.getFactorAccess().getVariableAssignment_0_2(), "rule__Factor__VariableAssignment_0_2");
			builder.put(grammarAccess.getFactorAccess().getVariableAssignment_1_0(), "rule__Factor__VariableAssignment_1_0");
			builder.put(grammarAccess.getFactorAccess().getSimboloAssignment_1_1(), "rule__Factor__SimboloAssignment_1_1");
			builder.put(grammarAccess.getFactorAccess().getValueAssignment_1_2(), "rule__Factor__ValueAssignment_1_2");
			builder.put(grammarAccess.getFactorAccess().getVariableAssignment_2(), "rule__Factor__VariableAssignment_2");
			builder.put(grammarAccess.getSetAccess().getNameAssignment_1(), "rule__Set__NameAssignment_1");
			builder.put(grammarAccess.getSetAccess().getElementsAssignment_4_0_0(), "rule__Set__ElementsAssignment_4_0_0");
			builder.put(grammarAccess.getSetAccess().getElementsAssignment_4_0_1_1(), "rule__Set__ElementsAssignment_4_0_1_1");
			builder.put(grammarAccess.getSetAccess().getLowerBoundAssignment_4_1_0(), "rule__Set__LowerBoundAssignment_4_1_0");
			builder.put(grammarAccess.getSetAccess().getUpperBoundAssignment_4_1_2(), "rule__Set__UpperBoundAssignment_4_1_2");
			builder.put(grammarAccess.getTupleAccess().getElementsAssignment_2_0(), "rule__Tuple__ElementsAssignment_2_0");
			builder.put(grammarAccess.getTupleAccess().getElementsAssignment_2_1_1(), "rule__Tuple__ElementsAssignment_2_1_1");
			builder.put(grammarAccess.getElementAccess().getValueAssignment_0(), "rule__Element__ValueAssignment_0");
			builder.put(grammarAccess.getElementAccess().getStringAssignment_1(), "rule__Element__StringAssignment_1");
			builder.put(grammarAccess.getElementAccess().getVariableAssignment_2(), "rule__Element__VariableAssignment_2");
			builder.put(grammarAccess.getElementAccess().getTupleAssignment_3(), "rule__Element__TupleAssignment_3");
			builder.put(grammarAccess.getVariableAccess().getNameAssignment_0(), "rule__Variable__NameAssignment_0");
			builder.put(grammarAccess.getVariableAccess().getIndexAssignment_1_1_0(), "rule__Variable__IndexAssignment_1_1_0");
			builder.put(grammarAccess.getVariableAccess().getIndexAssignment_1_1_1_1(), "rule__Variable__IndexAssignment_1_1_1_1");
			builder.put(grammarAccess.getVariableAccess().getIndexAssignment_1_1_1_2_1(), "rule__Variable__IndexAssignment_1_1_1_2_1");
		}
	}
	
	@Inject
	private NameMappings nameMappings;

	@Inject
	private DrakeGrammarAccess grammarAccess;

	@Override
	protected InternalDrakeParser createParser() {
		InternalDrakeParser result = new InternalDrakeParser(null);
		result.setGrammarAccess(grammarAccess);
		return result;
	}

	@Override
	protected String getRuleName(AbstractElement element) {
		return nameMappings.getRuleName(element);
	}

	@Override
	protected String[] getInitialHiddenTokens() {
		return new String[] { "RULE_WS", "RULE_ML_COMMENT", "RULE_SL_COMMENT" };
	}

	public DrakeGrammarAccess getGrammarAccess() {
		return this.grammarAccess;
	}

	public void setGrammarAccess(DrakeGrammarAccess grammarAccess) {
		this.grammarAccess = grammarAccess;
	}
	
	public NameMappings getNameMappings() {
		return nameMappings;
	}
	
	public void setNameMappings(NameMappings nameMappings) {
		this.nameMappings = nameMappings;
	}
}
