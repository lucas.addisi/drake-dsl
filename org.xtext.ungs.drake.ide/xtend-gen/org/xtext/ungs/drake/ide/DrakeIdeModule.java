/**
 * generated by Xtext 2.15.0
 */
package org.xtext.ungs.drake.ide;

import org.xtext.ungs.drake.ide.AbstractDrakeIdeModule;

/**
 * Use this class to register ide components.
 */
@SuppressWarnings("all")
public class DrakeIdeModule extends AbstractDrakeIdeModule {
}
